﻿using Inspection.DAL.Models;
using Inspection.DAL.Data;
using System;
using System.Collections.Generic;
using System.Text;
namespace Inspection.BLL
{
    public class PoliciesBLL
    {
        #region
        //private PoliciesData _dbP;

        //public PoliciesBLL(PoliciesData dbP) {
        //    _dbP = dbP;
        //}

        //public List<PoliciesVM> GetPolicies()
        //{
        //    return _dbP.GetPolicies("2022-12-01","2022-01-01");
        //}
        #endregion
        private PoliciesData _objPolicy;

        public PoliciesBLL() {
            _objPolicy = new PoliciesData();
        }

        public List<PoliciesVM> GetPoliciesInbox(DateTime startdate, DateTime enddate)
        {
            return _objPolicy.GetPoliciesInbox(startdate.ToString("yyyy-MM-dd HH:mm"), enddate.ToString("yyyy-MM-dd HH:mm"));
        }

    }
}
