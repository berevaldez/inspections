﻿using Inspection.Models;
using Inspection.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Inspection.BLL;
using Microsoft.AspNetCore.Http;

namespace Inspection.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private PoliciesBLL _policiesBLL;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            
        }

        public IActionResult Index()
        {
            _policiesBLL = new PoliciesBLL();

            List<PoliciesVM> PoliciesList = _policiesBLL.GetPoliciesInbox(Convert.ToDateTime("2021-12-01"), DateTime.Now);

            return View(PoliciesList);
        }


        public ActionResult Create(string PolicyNumber)
        {
            
            return RedirectToAction("Index");
        }

        // POST: Home/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
