﻿using System.Collections.Generic;
using Dapper;  
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Inspection.DAL
{
    public  class SqlDataAccess
    {
        /*
        public static string GetConnecctionString(string connectionName = "Default")
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }

*/

        private readonly IConfiguration _config;

        public string ConnectionStringName { get; set; } = "Default";

        public SqlDataAccess(IConfiguration config)
        {
            _config = config;
        }

        /*
        public static List<T> LoadData<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnecctionString()))
            {
                return cnn.Query<T>(sql).ToList();
            }
        }
        */

        public List<T> LoadData<T, U>(string sql, U parameters)
        {
            string connectionString = _config.GetConnectionString(ConnectionStringName);

            using IDbConnection connection = new SqlConnection(connectionString);
            var data = connection.Query<T>(sql, parameters);

            return data.ToList();
        }

        /*
        public static int SaveDate<T>(string sql, T data)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnecctionString()))
            {
                return cnn.Execute(sql,data);
            }
        }*/
        public int SaveData<T>(string sql, T parameters)
        {
            string connectionString = _config.GetConnectionString(ConnectionStringName);
            using IDbConnection connection = new SqlConnection(connectionString);
            return connection.Execute(sql, parameters);
        }


    }
}
