﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.DbAccess
{
    public interface ISqlDataAccess
    {
        List<T> LoadData<T, U>(string sql, U parameters);
        int SaveData<T>(string sql, T parameters);
    }
}
