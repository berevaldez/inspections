﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Inspection.DAL.DbAccess;
using Inspection.DAL.Models;

namespace Inspection.DAL.Data
{
    public class PoliciesData
    {
        #region
        //private readonly ISqlDataAccess _db;

        //public PoliciesData(ISqlDataAccess db)
        //{
        //    _db = db;
        //}


        //public List<PoliciesVM> GetPolicies(string StartDate, string EndDate)
        //{
        //    var sql = @"select PolicyNumber,CreatedOn as BindDate,EffectiveDateFrom, from Policies where createdOn BETWEEN @StartDate AND @EndDate";
        //    return _db.LoadData<PoliciesVM, dynamic>(sql, new { StartDate = StartDate, EndDate = EndDate });
        //}
        #endregion

        private static string connectionString = @"Data Source=tcp:trans-group.database.windows.net;Initial Catalog=TRANSP_GROUP_QQUOTE_DB;User ID=itmanager; Password=r3dL@mb0";

        public List<PoliciesVM> GetPoliciesByDate(string startdate,string enddate)
        {
            List<PoliciesVM> list = new List<PoliciesVM>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();
                SqlCommand query = new SqlCommand();
                query.Connection = conn;
                query.CommandText = @"SELECT p.Id,PolicyNumber,QQ.RequestNumber,CT.Type AS CoverageName,ICC.CompanyName,p.CreatedOn,EffectiveDateFrom,p.Status as PolicyStatus,PaymentMethod
                                           ,I.InspectionStatusId,CASE WHEN I.InspectionStatusId = NULL THEN 'New' ELSE InsS.Status END AS InspectionStatus
                                      FROM policies P
                                          INNER JOIN QuickQuotes QQ ON P.QuoteRequestId = qq.Id 
                                          INNER JOIN CoverageTypes CT ON P.CoverageTypeId =CT.ID
                                          INNER JOIN InsuranceCarrierCompanies ICC ON P.CarrierId =ICC.Id
                                          LEFT JOIN Inspections I ON P.Id = I.PolicyId
                                          LEFT JOIN InspectionStatus InsS ON I.InspectionStatusId=InsS.InspectionStatusId
                                      WHERE CreatedOn BETWEEN'" + startdate + "' AND '" + enddate + "'";
                try
                {
                    using (SqlDataReader reader = query.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var policuvar = new PoliciesVM
                            {
                                PolicyNumber = reader["PolicyNumber"].ToString(),
                                QuoteNumber = reader["RequestNumber"].ToString(),
                                Coverage = reader["CoverageName"].ToString(),
                                Carrier = reader["CompanyName"].ToString(),
                                BindDate = Convert.ToDateTime(reader["CreatedOn"].ToString()),
                                EffectiveDateFrom = Convert.ToDateTime(reader["EffectiveDateFrom"].ToString()),
                                PolicyStatus = reader["PolicyStatus"].ToString(),
                                PaymentMethod = reader["PaymentMethod"].ToString()
                                //FirmQuotes = Convert.ToInt32(reader["FQ"].ToString()),
                                // LastContacted = string.IsNullOrEmpty(reader["LastContacted"].ToString()) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(reader["LastContacted"].ToString())
                            };

                            list.Add(policuvar);
                        }
                        conn.Close();

                        return list;
                    }
                }
                catch (SqlException exp)
                {
                    return list;
                }
            }
        }

        public List<PoliciesVM> GetPoliciesInbox(string startdate, string enddate)
        {
            List<PoliciesVM> list = new List<PoliciesVM>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();
                SqlCommand query = new SqlCommand();
                query.Connection = conn;
                query.CommandText = @"SELECT p.Id,PolicyNumber,QQ.RequestNumber,CT.Type AS CoverageName,ICC.CompanyName,p.CreatedOn,EffectiveDateFrom,p.Status as PolicyStatus,PaymentMethod
                                           ,I.InspectionStatusId,CASE WHEN I.InspectionStatusId = NULL THEN 'New' ELSE InsS.Status END AS InspectionStatus
                                      FROM policies P
                                          INNER JOIN QuickQuotes QQ ON P.QuoteRequestId = qq.Id 
                                          INNER JOIN CoverageTypes CT ON P.CoverageTypeId =CT.ID
                                          INNER JOIN InsuranceCarrierCompanies ICC ON P.CarrierId =ICC.Id
                                          LEFT JOIN Inspections I ON P.Id = I.PolicyId
                                          LEFT JOIN InspectionStatus InsS ON I.InspectionStatusId=InsS.InspectionStatusId
                                      WHERE I.InspectionId IS NULL ";
                try
                {
                    using (SqlDataReader reader = query.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var policuvar = new PoliciesVM
                            {
                                PolicyNumber = reader["PolicyNumber"].ToString(),
                                QuoteNumber = reader["RequestNumber"].ToString(),
                                Coverage = reader["CoverageName"].ToString(),
                                Carrier = reader["CompanyName"].ToString(),
                                BindDate = Convert.ToDateTime(reader["CreatedOn"].ToString()),
                                EffectiveDateFrom = Convert.ToDateTime(reader["EffectiveDateFrom"].ToString()),
                                PolicyStatus = reader["PolicyStatus"].ToString(),
                                PaymentMethod = reader["PaymentMethod"].ToString()
                                //FirmQuotes = Convert.ToInt32(reader["FQ"].ToString()),
                                // LastContacted = string.IsNullOrEmpty(reader["LastContacted"].ToString()) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(reader["LastContacted"].ToString())
                            };

                            list.Add(policuvar);
                        }
                        conn.Close();

                        return list;
                    }
                }
                catch (SqlException exp)
                {
                    return list;
                }
            }
        }






    }
}
