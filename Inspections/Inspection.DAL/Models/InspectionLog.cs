﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.Models
{
    public class InspectionLog
    {
        public Guid InspectionLogId { get; set; }
        public Guid InspectionId { get; set; }
        public Guid InspectionUserId { get; set; }
        public string Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
