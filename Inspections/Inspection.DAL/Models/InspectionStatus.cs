﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class InspectionStatus
    {
        public InspectionStatus()
        {
            Inspections = new HashSet<Inspections>();
        }

        public Guid InspectionStatusId { get; set; }
        public string Status { get; set; }
        public bool? Isactive { get; set; }

        public virtual ICollection<Inspections> Inspections { get; set; }
    }
}
