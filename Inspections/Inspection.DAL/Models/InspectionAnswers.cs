﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class InspectionAnswers
    {
        public Guid InspectionAnswerId { get; set; }
        public Guid InspectionQuestionId { get; set; }
        public string Answer { get; set; }
        public bool IsActive { get; set; }
        public int AnswerOrder { get; set; }

        public virtual InspectionQuestions InspectionQuestion { get; set; }
    }
}
