﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class InspectionDetails
    {
        public Guid InspectionDetailId { get; set; }
        public Guid InspectionId { get; set; }
        public Guid InspectionQuestionId { get; set; }
        public Guid InspectionAnswerId { get; set; }
        public string Response { get; set; }
        public bool IsMultipleOption { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Inspections Inspection { get; set; }
        public virtual InspectionQuestions InspectionQuestion { get; set; }
    }
}
