﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class InspectionUsers
    {
        public InspectionUsers()
        {
            Inspections = new HashSet<Inspections>();
        }

        public Guid UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastlame { get; set; }
        public string Email { get; set; }
        public bool Isactive { get; set; }
        public DateTime Createdon { get; set; }
        public DateTime? Lastupdated { get; set; }

        public virtual ICollection<Inspections> Inspections { get; set; }
    }
}
