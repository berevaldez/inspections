﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class Inspections
    {
        public Inspections()
        {
            InspectionDetails = new HashSet<InspectionDetails>();
        }

        public Guid InspectionId { get; set; }
        public Guid PolicyId { get; set; }
        public Guid InspectionUserId { get; set; }
        public Guid InspectionStatusId { get; set; }
        public string Comments { get; set; }
        public string CanceledReason { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string LastModifiedBy { get; set; }

        public virtual InspectionStatus InspectionStatus { get; set; }
        public virtual InspectionUsers InspectionUser { get; set; }
        public virtual ICollection<InspectionDetails> InspectionDetails { get; set; }
    }
}
