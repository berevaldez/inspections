﻿using System;
using System.Collections.Generic;

namespace Inspection.DAL.Models
{
    public partial class InspectionQuestions
    {
        public InspectionQuestions()
        {
            InspectionAnswers = new HashSet<InspectionAnswers>();
            InspectionDetails = new HashSet<InspectionDetails>();
        }

        public Guid InspectionQuestionId { get; set; }
        public string Question { get; set; }
        public bool IsMultipleOption { get; set; }
        public bool IsObligatory { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string QuestionOrder { get; set; }
        public virtual ICollection<InspectionAnswers> InspectionAnswers { get; set; }
        public virtual ICollection<InspectionDetails> InspectionDetails { get; set; }
    }
}
