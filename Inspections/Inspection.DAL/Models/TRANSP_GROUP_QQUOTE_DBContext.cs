﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Inspection.DAL.Models
{
    public partial class TRANSP_GROUP_QQUOTE_DBContext : DbContext
    {
        public TRANSP_GROUP_QQUOTE_DBContext()
        {
        }

        public TRANSP_GROUP_QQUOTE_DBContext(DbContextOptions<TRANSP_GROUP_QQUOTE_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InspectionAnswers> InspectionAnswers { get; set; }
        public virtual DbSet<InspectionDetails> InspectionDetails { get; set; }
        public virtual DbSet<InspectionQuestions> InspectionQuestions { get; set; }
        public virtual DbSet<InspectionStatus> InspectionStatus { get; set; }
        public virtual DbSet<InspectionUsers> InspectionUsers { get; set; }
        public virtual DbSet<Inspections> Inspections { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=tcp:trans-group.database.windows.net,1433;Initial Catalog=TRANSP_GROUP_QQUOTE_DB;User Id=itmanager;Password=r3dL@mb0;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InspectionAnswers>(entity =>
            {
                entity.HasKey(e => e.InspectionAnswerId);

                entity.Property(e => e.InspectionAnswerId).ValueGeneratedNever();

                entity.Property(e => e.Answer)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.HasOne(d => d.InspectionQuestion)
                    .WithMany(p => p.InspectionAnswers)
                    .HasForeignKey(d => d.InspectionQuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InspectionAnswers_InspectionQuestions");
            });

            modelBuilder.Entity<InspectionDetails>(entity =>
            {
                entity.HasKey(e => e.InspectionDetailId);

                entity.Property(e => e.InspectionDetailId).ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsMultipleOption).HasColumnName("isMultipleOption");

                entity.Property(e => e.Response)
                    .IsRequired()
                    .HasColumnName("response")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.HasOne(d => d.Inspection)
                    .WithMany(p => p.InspectionDetails)
                    .HasForeignKey(d => d.InspectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InspectionDetails_Inspections");

                entity.HasOne(d => d.InspectionQuestion)
                    .WithMany(p => p.InspectionDetails)
                    .HasForeignKey(d => d.InspectionQuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InspectionDetails_InspectionQuestions");
            });

            modelBuilder.Entity<InspectionQuestions>(entity =>
            {
                entity.HasKey(e => e.InspectionQuestionId);

                entity.Property(e => e.InspectionQuestionId).ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.IsMultipleOption).HasColumnName("isMultipleOption");

                entity.Property(e => e.IsObligatory).HasColumnName("isObligatory");

                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InspectionStatus>(entity =>
            {
                entity.Property(e => e.InspectionStatusId).ValueGeneratedNever();

                entity.Property(e => e.Isactive)
                    .IsRequired()
                    .HasColumnName("isactive")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InspectionUsers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_InspectionsUsers");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Createdon)
                    .HasColumnName("createdon")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(60)
                    .IsFixedLength();

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Lastlame)
                    .IsRequired()
                    .HasColumnName("lastlame")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Lastupdated)
                    .HasColumnName("lastupdated")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Inspections>(entity =>
            {
                entity.HasKey(e => e.InspectionId);

                entity.Property(e => e.InspectionId).ValueGeneratedNever();

                entity.Property(e => e.CanceledReason).HasMaxLength(200);

                entity.Property(e => e.Comments).HasMaxLength(500);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.InspectionStatus)
                    .WithMany(p => p.Inspections)
                    .HasForeignKey(d => d.InspectionStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Inspections_InspectionStatus");

                entity.HasOne(d => d.InspectionUser)
                    .WithMany(p => p.Inspections)
                    .HasForeignKey(d => d.InspectionUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Inspections_InspectionUsers");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
