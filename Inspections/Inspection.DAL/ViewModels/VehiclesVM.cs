﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.ViewModels
{
    public class VehiclesVM
    {
        public Guid VehicleId { get; set; }
        public string VehicleType { get; set; }
        public int VehicleYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VIN { get; set; }

        public string Notes { get; set; }

    }
}
