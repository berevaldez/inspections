﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.ViewModels
{
    public class DriversVM
    {
        public Guid InsuredId { get; set; }
        public string DriverName { get; set; }
        public bool isOwner { get; set; }
        public string DOB { get; set; }
        public string LicenseNumber { get; set; }

        public DateTime? IssuedDate { get; set; }
    }
}
