﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.ViewModels
{
    public class CoverageVM
    {
        public string CoverageName { get; set; }
        public double Deductible { get; set; }
        public string Carrier { get; set; }
        public string UMorUIM { get; set; }
    }
}
