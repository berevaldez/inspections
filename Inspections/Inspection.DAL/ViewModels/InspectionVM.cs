﻿using System;
using System.Collections.Generic;
using System.Text;
using Inspection.DAL.Models;
using System.Linq;

namespace Inspection.DAL.ViewModels
{
    public class InspectionVM
    {
        public InspectionVM() { 
            Questions = new List<InspectionQuestions>();
            Status = new InspectionStatus();
        }
        public string PolicyNumber { get; set; }
        public string User { get; set; }
        public InsuredVM Insured { get; set; }
        public CoverageVM Coverage { get; set; }
        public VehiclesVM Vehicles { get; set; }
        public DriversVM Drivers { get; set; }
        public List<InspectionQuestions> Questions
        {
            get;set;
        }
        public InspectionStatus Status { get; set; }

        public string Notes { get; set; }
    }
}
