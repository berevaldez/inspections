﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.ViewModels
{
    public class InsuredVM
    {
        public Guid InsuredId { get; set; }
        public string InsuredName { get; set; }
        public string DBA {get; set; }
        public string State { get; set; }
        public string RadiusOperation { get; set; }
        public string NatureOfBusiness { get; set; }
        public string Commodities { get; set; }
        public string MailingAddress { get; set; }
        public string GaragingAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Filings { get; set; }
        
    }
}
