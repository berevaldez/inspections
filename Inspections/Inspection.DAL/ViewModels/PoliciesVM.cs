﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.Models
{
    public class PoliciesVM
    {
        public string PolicyNumber { get; set; }
        public string QuoteNumber { get; set; }
        public string Coverage { get; set; }
        public string Carrier { get; set; }
        public DateTime BindDate { get; set; }
        public DateTime EffectiveDateFrom { get; set; }
        public string PolicyStatus { get; set; }
        public string PaymentMethod { get; set; }

    }
}
