﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspection.DAL.ViewModels
{
    class InspectionDashboardVM
    {
        public int New { get; set; }
        public int Pending { get; set; }
        public int Completed { get; set; }

        public int Canceled { get; set; }


    }
}
